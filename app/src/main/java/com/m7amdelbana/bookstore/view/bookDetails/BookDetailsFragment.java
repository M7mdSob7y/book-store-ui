package com.m7amdelbana.bookstore.view.bookDetails;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.m7amdelbana.bookstore.R;
import com.m7amdelbana.bookstore.databinding.FragmentBookDetailsBinding;

public class BookDetailsFragment extends Fragment {

    private FragmentBookDetailsBinding binding;


    public BookDetailsFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_book_details, container, false);
        return binding.getRoot();
    }
}
package com.m7amdelbana.bookstore.view.splash;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.m7amdelbana.bookstore.R;
import com.m7amdelbana.bookstore.databinding.FragmentSplashBinding;

public class SplashFragment extends Fragment {

    private static final int SPLASH_TIME_OUT = 3000;

    private FragmentSplashBinding binding;

    public SplashFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_splash, container, false);
        new Handler().postDelayed(() -> {
            NavHostFragment.findNavController(this).popBackStack(R.id.splashFragment, true);
            NavHostFragment.findNavController(this).navigate(R.id.actionHome);
        }, SPLASH_TIME_OUT);
        return binding.getRoot();
    }
}

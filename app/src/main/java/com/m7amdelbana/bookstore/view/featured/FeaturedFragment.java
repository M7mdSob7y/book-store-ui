package com.m7amdelbana.bookstore.view.featured;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.m7amdelbana.bookstore.R;
import com.m7amdelbana.bookstore.databinding.FragmentFeaturedBinding;

public class FeaturedFragment extends Fragment {

    private FragmentFeaturedBinding binding;

    public FeaturedFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_featured, container, false);
        return binding.getRoot();
    }
}

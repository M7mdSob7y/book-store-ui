package com.m7amdelbana.bookstore.view.main;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import com.m7amdelbana.bookstore.R;
import com.m7amdelbana.bookstore.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setupNavigation();
    }

    private void setupNavigation() {
        NavController navController = Navigation.findNavController(this, R.id.navHostFragment);
        NavigationUI.setupWithNavController(binding.navigationView, navController);
        navController.addOnDestinationChangedListener((controller, destination, arguments) -> {
            if (destination.getLabel().equals(getString(R.string.splash))) {
                binding.navigationView.setVisibility(View.INVISIBLE);
            } else {
                binding.navigationView.setVisibility(View.VISIBLE);
            }

        });
    }


}

